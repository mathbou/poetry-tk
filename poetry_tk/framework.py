#
# Copyright (c) 2022 Mathieu Bouzard.
#
# This file is part of poetry-tk
# (see https://gitlab.com/mathbou/poetry-tk).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from __future__ import annotations

__all__ = [
    "Framework",
    "load_from_pyproject",
    "load_from_info",
    "install",
    "remove",
    "sync",
    "resolve",
]

import re
import shlex
import shutil
import subprocess
import sys
import tempfile
from dataclasses import dataclass
from pathlib import Path
from typing import TYPE_CHECKING, Any, Optional

import dulwich.porcelain
import tomlkit
from dulwich.errors import NotGitRepository
from dulwich.repo import Repo
from poetry.core.pyproject.toml import PyProjectTOML
from poetry.core.semver.helpers import parse_constraint
from poetry.core.semver.version import Version
from poetry.core.version.exceptions import InvalidVersion
from poetry.mixology.incompatibility_cause import NoVersionsCause
from poetry.utils.env import EnvManager
from poetry.vcs.git import Git

from poetry_tk import constants as cst
from poetry_tk import info
from poetry_tk.exceptions import FrameworkSolvingError
from poetry_tk.settings import Settings

if TYPE_CHECKING:
    from collections.abc import Iterable

    from poetry.core.semver.version_constraint import VersionConstraint
    from poetry.poetry import Poetry


ORG_URL = "https://github.com/shotgunsoftware"


@dataclass
class Framework:
    name: str
    version: VersionConstraint | Version | str
    git: str = None
    path: Path = None
    info_version: str = None
    parent: Framework = None

    def __post_init__(self):
        if self.git is not None and self.path is not None:
            raise RuntimeError("git and path options cant be used at the same time.")
        elif self.git is None and self.path is None:
            self.git = f"{ORG_URL}/{self.name}.git"

        if self.path:
            self.version = parse_constraint(_get_local_version(self.path))
        elif isinstance(self.version, str):
            self.version = parse_constraint(self.version)

        if self.info_version is not None:
            re_v_part = r"(?:\d+|x)"
            if not re.match(rf"^v?{re_v_part}(?:\.{re_v_part})*$", self.info_version, flags=re.IGNORECASE):
                raise RuntimeError("Invalid format: info_version")

            self.info_version = str(self.info_version)

    @property
    def is_local(self) -> bool:
        return self.path is not None

    @property
    def package_name(self) -> str:
        return self.name.replace("-", "_")

    @property
    def refs(self) -> list[str]:
        if self.is_local:
            return []

        raw_refs = dulwich.porcelain.ls_remote(self.git)
        return [ref.decode("utf8") for ref in raw_refs]

    @property
    def versions(self) -> list[Version]:
        tags = []
        for ref in reversed(self.refs):
            ref = ref.replace("refs/tags/", "")

            try:
                tags.append(Version.parse(ref))
            except InvalidVersion:
                pass

        return tags

    @property
    def allowed_versions(self) -> list[Version]:
        return [version for version in self.versions if self.version.allows(version)]

    @property
    def last_allowed_version(self) -> Optional[Version]:  # noqa: FNE002
        if self.is_local:
            return self.version

        try:
            return sorted(self.allowed_versions, reverse=True)[0]
        except IndexError:
            return None

    @property
    def min_allowed_version(self) -> Optional[Version]:  # noqa: FNE002
        if self.is_local:
            return self.version

        try:
            return sorted(self.allowed_versions)[0]
        except IndexError:
            return None

    def clone_into(self, root: Path) -> Repo:
        if self.is_local:
            sys.stdout.write(f"Local repo {self.name} not cloned.\n")
            return Repo(str(self.path))

        try:
            Repo(str(root / self.name))
            clean = False
        except NotGitRepository:
            clean = True

        last_version = self.last_allowed_version

        if last_version is None:
            raise NoVersionsCause(f"Cant clone {self.name}. Cant find any version matching {self.version}.")

        return Git.clone(self.git, tag=str(last_version), source_root=root, clean=clean)

    def get_info_version(self) -> str:
        if self.info_version is not None:
            return self.info_version
        elif self.is_local:
            return str(self.version)
        else:
            v_min = self.allowed_versions[-1]
            v_max = self.allowed_versions[0]

            patch = "x"
            if v_max.patch == v_min.patch:
                patch = v_max.patch

            minor = "x"
            if v_max.minor == v_min.minor:
                minor = v_max.minor

            major = "x"
            if v_max.major == v_min.major:
                major = v_max.major

            return f"v{major}.{minor}.{patch}"

    def as_info(self) -> dict[str, Any]:
        return {
            cst.name: self.name,
            cst.version: self.get_info_version(),
            "minimum_version": str(self.min_allowed_version or ""),
        }

    def as_poetry(self) -> (str, dict[str, str]):
        spec = {cst.version: str(self.version), "git": self.git}
        return self.name, spec

    def get_dependencies(self) -> list[Framework]:
        repo = self.clone_into(Path(tempfile.gettempdir()))

        pyproject_data = Settings.load_from_pyproject(PyProjectTOML(Path(repo.path, "pyproject.toml")))
        if pyproject_data.frameworks:
            frameworks = load_from_pyproject(pyproject_data.frameworks)
        else:
            info_data = info.load_from_file(Path(repo.path, "info.yml"))
            frameworks = load_from_info(info_data)

        for framework in frameworks:
            framework.parent = self
        return frameworks


# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------


def load_from_info(info_data: dict) -> list[Framework]:
    frameworks = []
    for spec in info_data.get(cst.frameworks) or []:
        version = spec[cst.version].replace("x", "*").strip("v")

        min_version = spec.get("minimum_version")
        if min_version:
            min_version.replace("x", "*").strip("v")
            version += f",>={min_version}"

        frameworks.append(Framework(name=spec[cst.name], version=version))

    return frameworks


def load_from_pyproject(pyproject_data: dict) -> list[Framework]:
    frameworks = []

    for name, options in pyproject_data.items():
        if not isinstance(options, dict):
            options = {cst.version: options}

        elif cst.path in options:
            options[cst.version] = "*"

        frameworks.append(Framework(name=name, **options))

    return frameworks


def _get_local_version(path: Path) -> str:
    """Try to find framework version from pyproject or __version__ var.

    Args:
        path: Local framework path
    """
    pyproject = Path(path, "pyproject.toml")

    if pyproject.exists():
        data = tomlkit.loads(pyproject.read_text(encoding="utf8"))
        return data[cst.tool]["poetry"][cst.version]
    else:
        for dunder_file in Path(path).rglob("__*__.py"):
            text = dunder_file.read_text(encoding="utf8")
            match = re.findall("__version__ ?= ?[\"'](.+?)[\"']", text)
            if match:
                return match[0]

    return "0.0.0"


def _installed(venv: Path) -> dict[str, list[Path]]:
    """List installed frameworks."""
    src = venv / "src"
    site = venv / "lib" / "site-packages"

    installed_frameworks = {}
    for item in site.glob("*/.tk-framework"):
        f_name = item.read_text(encoding="utf8")

        installed_frameworks[f_name] = [item.parent]

        src_framework = src / f_name
        if src_framework.exists():
            installed_frameworks[f_name].append(src_framework)

    return installed_frameworks


def install(venv: Path, frameworks: list[Framework]):
    """Install frameworks in venv."""
    src = venv / "src"
    site = venv / "lib" / "site-packages"

    def link_from_src_to_site(framework: Framework, py_src: Path):
        sys.stdout.write(f"Linking {framework.name} into site-packages as {framework.package_name}\n")

        dst = site / framework.package_name

        if dst.exists():
            shutil.rmtree(dst)

        try:
            py_src.symlink_to(dst, target_is_directory=True)
        except OSError:
            shutil.copytree(py_src, dst)
        finally:
            data = dst / ".tk-framework"
            data.write_text(framework.name, encoding="utf8")

    # ------------------------------------------------------------------------

    try:
        resolved_frameworks = resolve(frameworks)
    except FrameworkSolvingError:
        raise
    else:
        for framework in resolved_frameworks:
            sys.stdout.write(f"Cloning {framework.name} {framework.last_allowed_version}\n")

            repo = framework.clone_into(src)

            std_pkg_path = Path(repo.path, framework.package_name)
            if std_pkg_path.exists():
                py_src = std_pkg_path
            else:
                py_src = Path(repo.path, "python")

            if py_src.exists():
                link_from_src_to_site(framework, py_src)


def remove(venv: Path, framework_names: Iterable[str]):
    """Remove frameworks from venv."""
    installed_frameworks = _installed(venv)

    for f_name, framework_paths in installed_frameworks.items():
        if f_name in framework_names:
            sys.stdout.write(f"Remove {f_name}\n")
            for path in framework_paths:
                shutil.rmtree(path)


def sync(pyproject_data: dict, venv: Path):
    frameworks = load_from_pyproject(pyproject_data)
    f_names = [f.name for f in frameworks]

    installed_frameworks = set(_installed(venv))

    remove(venv, installed_frameworks.difference(f_names))

    install(venv, frameworks)


def resolve(frameworks: list[Framework]) -> list[Framework]:
    all_frameworks = {}

    def nested_resolution(frameworks_: list[Framework]):
        for framework in frameworks_:
            try:
                all_frameworks[framework.name].append(framework)
            except KeyError:
                all_frameworks[framework.name] = [framework]

            nested_resolution(framework.get_dependencies())

    nested_resolution(frameworks)

    resolved_frameworks = []
    errors = []

    for name, frameworks in all_frameworks.items():
        if len(frameworks) == 1:
            resolved_frameworks.append(frameworks[0])
            continue

        for framework in frameworks:
            try:
                allowed_version.intersection_update(framework.allowed_versions)
            except NameError:
                allowed_version = set(framework.allowed_versions)

        if not allowed_version:
            constraint = []
            for f in frameworks:
                if f.parent:
                    constraint.append(f"{f.version} from {f.parent.name}")
                else:
                    constraint.append(f"{f.version} from current project")
            constraint = "\n\t- ".join(constraint)

            errors.append(f"Cant resolve {name}: \n\t- {constraint}")
        else:
            str_versions = [v.to_string() for v in sorted(allowed_version)]
            framework.version = parse_constraint("|".join(str_versions))
            resolved_frameworks.append(framework)

    if errors:
        raise FrameworkSolvingError(errors)

    return resolved_frameworks


def fix_tk_hook_installation(poetry: Poetry):
    sys.stdout.write("Linking tk-core hook into venv lib folder\n")

    venv = EnvManager(poetry).get()
    tk_hook_src = venv.path / "src" / cst.sgtk / "hooks"
    dst = venv.path / "lib" / "hooks"

    if dst.exists():
        shutil.rmtree(dst)

    try:
        tk_hook_src.symlink_to(dst, target_is_directory=True)
    except OSError:
        shutil.copytree(tk_hook_src, dst)


def install_tk_core(poetry: Poetry, version: str = None) -> str:
    venv = EnvManager(poetry).get()
    if not venv.is_venv():
        raise RuntimeError("Poetry venv must be initialized to install tk-core.")

    project_path = poetry.file.path.parent

    cmd = "poetry run pip -qq install -e git+https://github.com/shotgunsoftware/tk-core.git"

    if version:
        version = parse_constraint(version).min
        version_str = f"{version.major}.{version.minor}.{version.patch or 0}"
        cmd += f"@v{version_str}"
        sys.stdout.write(f"Update tk-core to version {version_str}\n")
    else:
        sys.stdout.write("Update tk-core to last version\n")

    cmd += f"#egg={cst.sgtk}"

    try:
        subprocess.check_call(shlex.split(cmd), cwd=project_path)
    except subprocess.CalledProcessError as e:
        raise RuntimeError("Couldn't install/update tk-core.") from e

    fix_tk_hook_installation(poetry)

    if not version:
        result = subprocess.check_output(
            "poetry run pip --disable-pip-version-check list -e --format freeze", cwd=project_path, text=True
        )
        for line in result.splitlines():
            if line.startswith("sgtk=="):
                return line.split("==")[1].strip()

        raise RuntimeError("Failed to retrieve tk-core version after installation")

    return version_str
