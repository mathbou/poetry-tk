#
# Copyright (c) 2022 Mathieu Bouzard.
#
# This file is part of poetry-tk
# (see https://gitlab.com/mathbou/poetry-tk).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from __future__ import annotations

__all__ = [
    "TkPlugin",
]

from typing import TYPE_CHECKING

from cleo.events.console_events import COMMAND
from poetry.plugins.application_plugin import ApplicationPlugin
from poetry.utils.env import EnvManager

from . import framework as fk
from . import info
from .commands import (  # noqa: DUO125
    FrameworkAddCommand,
    FrameworkListCommand,
    FrameworkLoaderCommand,
    FrameworkRemoveCommand,
    FrameworkResolveCommand,
    FrameworkSyncCommand,
)
from .settings import Settings

if TYPE_CHECKING:
    from cleo.events.console_command_event import ConsoleCommandEvent
    from cleo.events.event_dispatcher import EventDispatcher
    from poetry.console.application import Application


class TkPlugin(ApplicationPlugin):
    def activate(self, application: Application):
        self._app = application

        try:
            settings = Settings.load_from_pyproject(self._app.poetry.pyproject)
        except RuntimeError:
            return

        if settings.enable:
            application.command_loader.register_factory(FrameworkAddCommand.name, FrameworkAddCommand)
            application.command_loader.register_factory(FrameworkRemoveCommand.name, FrameworkRemoveCommand)
            application.command_loader.register_factory(FrameworkSyncCommand.name, FrameworkSyncCommand)
            application.command_loader.register_factory(FrameworkLoaderCommand.name, FrameworkLoaderCommand)
            application.command_loader.register_factory(FrameworkListCommand.name, FrameworkListCommand)
            application.command_loader.register_factory(FrameworkResolveCommand.name, FrameworkResolveCommand)

            application.event_dispatcher.add_listener(COMMAND, self._sync_frameworks)

    def _sync_frameworks(self, event: ConsoleCommandEvent, kind: str, dispatcher: EventDispatcher) -> None:
        if event.command.name == "install":
            self._settings = Settings.load_from_pyproject(self._app.poetry.pyproject)

            if event.io.input.option("dry-run"):
                event.io.write_line("Dry-Run tk-frameworks")

            elif self._settings.auto_sync is True:
                venv = EnvManager(self._app.poetry).get()
                frameworks = fk.load_from_pyproject(self._settings.frameworks)

                if event.io.input.option("sync"):
                    fk.sync(self._settings.frameworks, venv.path)
                elif not event.io.input.option("only-root"):
                    fk.install(venv.path, frameworks)

                if self._settings.update_info is True:
                    info.update(self._settings, frameworks)
