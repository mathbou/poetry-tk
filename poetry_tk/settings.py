#
# Copyright (c) 2022 Mathieu Bouzard.
#
# This file is part of poetry-tk
# (see https://gitlab.com/mathbou/poetry-tk).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from __future__ import annotations

__all__ = ["Settings"]

from dataclasses import dataclass, field
from typing import TYPE_CHECKING

from . import constants as cst

if TYPE_CHECKING:
    from pathlib import Path

    from poetry.core.pyproject.toml import PyProjectTOML
    from tomlkit import TOMLDocument


@dataclass
class Settings:
    _data: TOMLDocument = field(repr=False)
    _file: Path = field(repr=False)
    enable: bool = field(default=False)
    auto_sync: bool = field(default=False)
    update_info: bool = field(default=True)
    core_version: str = field(default=None)
    frameworks: dict = field(default_factory=dict)

    @classmethod
    def load_from_pyproject(cls, pyproject: PyProjectTOML) -> "Settings":
        data = pyproject.data

        try:
            tk_settings = data[cst.tool][cst.poetry_tk]
        except KeyError:
            return cls(data, pyproject.file.path)
        else:
            normalized_settings = {name.replace("-", "_"): key for name, key, in tk_settings.items()}
            return cls(data, pyproject.file.path, **normalized_settings)

    @property
    def data(self) -> TOMLDocument:
        return self._data

    @property
    def file(self) -> Path:
        return self._file
