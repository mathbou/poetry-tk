#
# Copyright (c) 2022 Mathieu Bouzard.
#
# This file is part of poetry-tk
# (see https://gitlab.com/mathbou/poetry-tk).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from __future__ import annotations

__all__ = ["load_from_file", "update"]

import contextlib
import sys
from typing import TYPE_CHECKING

from ruamel import yaml
from ruamel.yaml import CommentedMap

from . import constants as cst

if TYPE_CHECKING:
    from pathlib import Path

    from .framework import Framework
    from .settings import Settings


def load_from_file(path: Path) -> CommentedMap:
    with path.open(mode="r", encoding="utf8") as file:
        loader = yaml.YAML()
        loader.width = 120

        return loader.load(file) or CommentedMap()


@contextlib.contextmanager
def _file_updater(path: Path):
    info_path = path / "info.yml"
    info_path.touch(exist_ok=True)

    with (path / "info.yml").open(mode="r", encoding="utf8") as file:
        loader = yaml.YAML()
        loader.width = 120

        data = loader.load(file) or CommentedMap()

    yield data

    with (path / "info.yml").open(mode="w", encoding="utf8") as file:
        sys.stdout.write("Update info.yml\n")
        loader.dump(data, stream=file)


def update(pyproject: Settings, frameworks: list["Framework"], core_version: str = None):
    with _file_updater(pyproject.file.parent) as data:
        data["display_name"] = str(pyproject.data[cst.tool]["poetry"]["name"])
        data["description"] = str(pyproject.data[cst.tool]["poetry"]["description"])

        data[cst.frameworks] = yaml.CommentedSeq()

        for framework in frameworks:
            f_data = yaml.CommentedMap(framework.as_info())
            f_data.fa.set_flow_style()

            data[cst.frameworks].append(f_data)

        if core_version is not None:
            data["requires_core_version"] = str(core_version)
