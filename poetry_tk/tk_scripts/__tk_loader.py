#
# Copyright (c) 2022 Mathieu Bouzard.
#
# This file is part of poetry-tk
# (see https://gitlab.com/mathbou/poetry-tk).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

"""This file should be copy-pasted into shotgun apps/frameworks/engine that may be called first in a process.

This loader gives the ability to import frameworks as standard python modules.

    ```yaml
    📂 <app/framework/engine name>:
        - 📂 python:
            - 🐍 __init__.py: contains an import line for `__tk_loader`
            - 🐍 __tk_loader.py
            - 🐍 <your_module.py>
            - ...
        - 📝 info.yml:
        - ...
    ```

Warnings:
    The engineInit hook may clash with this loader, they are not meant to both run in the same environment.
"""

__all__ = []

import logging
import sys
from pathlib import Path
from typing import TYPE_CHECKING

import sgtk  # noqa: I900

if TYPE_CHECKING:
    from tank.platform import Framework  # noqa: I900

log = sgtk.platform.get_logger(__name__)


def _tk_register_module(log: logging.Logger, framework_name: str, package_name: str, module_name: str):
    """Pre-load module using sgtk methods.

    Args:
        log: logger to use
        framework_name: framework name as defined in shotgun's environment
        package_name: Specify under which package name the module will be registered.
        module_name: module name as defined in shotgun's environment
    """  # noqa: D417
    try:
        module = sgtk.platform.import_framework(framework_name, module_name)
    except sgtk.TankError as e:
        log.error(f"Cant register framework module: {e}")
    else:
        registering_name = f"{package_name}.{module_name}"
        log.info(f"Register framework module: {registering_name}")
        sys.modules[registering_name] = module


def _tk_register_frameworks(log: logging.Logger, info_path: Path):
    """Pre-load frameworks using sgtk methods.

    Args:
        log: logger to use
        info_path: path to info.yaml file describing frameworks to load
    """
    import importlib.util

    from tank_vendor import yaml  # noqa: I900

    with info_path.open(mode="r", encoding="utf8") as file:
        framework_specs = yaml.safe_load(file).get("frameworks")

    for framework_spec in framework_specs:
        f_name = framework_spec["name"]

        try:
            framework: Framework = sgtk.platform.get_framework(f_name)
        except sgtk.platform.errors.TankCurrentModuleNotFoundError as e:
            log.error(e)
        else:
            normalized_f_name = f_name.replace("-", "_")

            for src_path in Path(framework.disk_location, "python").iterdir():
                is_package = src_path.is_dir() and (src_path / "__init__.py").exists()
                is_module = src_path.is_file() and src_path.suffix == ".py"
                is_init = is_module and src_path.name == "__init__.py"
                is_magic = src_path.name.startswith("__")

                if is_init:
                    spec = importlib.util.spec_from_file_location(normalized_f_name, src_path)
                    mod = importlib.util.module_from_spec(spec)
                    log.info(f"Register framework: {normalized_f_name}")
                    sys.modules[normalized_f_name] = mod

                elif (is_package or is_module) and not is_magic:
                    _tk_register_module(log, f_name, normalized_f_name, src_path.stem)

                else:
                    log.debug(f"{normalized_f_name} is not a package nor a module, ignoring")


def _register_frameworks():
    """Pre-load frameworks to allow importing them as regular modules.

    Examples:

        # Instead of this
        import sgtk
        shotgun_model = sgtk.platform.import_framework("tk-framework-shotgunutils", "shotgun_model")
        shotgun_model.ShotgunHierarchyModel

        # Or this (into apps/engines)
        shotgun_model = self.frameworks["tk-framework-shotgunutils"].import_module("shotgun_model")
        shotgun_model.ShotgunHierarchyModel

        # Do this
        from tk_framework_shotgunutils.shotgun_model import ShotgunHierarchyModel

        # Or this
        from tk_framework_shotgunutils import shotgun_model

    """
    from unittest.mock import Mock

    import tank  # noqa: I900

    try:
        bundle = sgtk.platform.current_bundle()
        if isinstance(bundle, Mock):
            return  # already mocked
    except sgtk.platform.errors.TankCurrentModuleNotFoundError:
        # If current python env is virtualEnv
        tank.platform.util.current_bundle = Mock
        log.debug(f"Mock tank current_bundle function for : {__name__}")
    else:
        # If current python env is TK
        project_root = Path(__file__).parent.parent
        info_path = project_root / "info.yml"
        if not info_path.exists():
            log.error(f"Not found {info_path}")
            return
        else:
            _tk_register_frameworks(log, info_path)


# We want this to execute when it's imported so that when shotgun loads frameworks, apps and engines they become
# available as modules for other apps/engines/frameworks
_register_frameworks()
