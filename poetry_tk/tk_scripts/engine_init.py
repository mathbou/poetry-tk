#
# Copyright (c) 2022 Mathieu Bouzard.
#
# This file is part of poetry-tk
# (see https://gitlab.com/mathbou/poetry-tk).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

"""This file should be copy-pasted in the `tk-config/core/hooks` folder.

This hook gives the ability to use a standard python package architecture for apps and frameworks.

    ```yaml
        📂 <app/framework name>:
            - 📂 python:
                - 🐍 __init__.py: contains the variable `__tk_pre_load = True`
            - 📂 <pkg_name>:
                - 🐍 __init__.py
                - 🐍 <your_module.py>
                - ...
            - 📝 info.yml
            - ...
    ```

Warnings:
    __tk_loader may clash with this hook, they are not meant to both run in the same environment.

"""

from __future__ import annotations

import importlib.util
import re
import sys
from pathlib import Path
from typing import TYPE_CHECKING, Any

import sgtk  # noqa: I900
import tank  # noqa: I900
from tank import Hook  # noqa: I900

if TYPE_CHECKING:
    from importlib.machinery import ModuleSpec
    from types import ModuleType

    from tank.descriptor import FrameworkDescriptor  # noqa: I900
    from tank.platform.bundle import TankBundle  # noqa: I900
    from tank.platform.engine import Engine  # noqa: I900
    from tank.platform.environment import InstalledEnvironment  # noqa: I900

__all__ = ["EngineInit"]


class EngineInit(Hook):
    def execute(self, engine: Engine, **kwargs: Any):
        self.patch_tank_bundle()

        self.register_frameworks(engine)

    def conform_name(self, value: str) -> str:
        return re.sub("[ -]", "_", value)

    def need_preload(self, root_path: Path) -> bool:
        """Check if `__tk_pre_load` variable is present in the root `__init__.py` file and set to True."""
        is_sgtk_pkg = root_path.name == "python"
        init_module = root_path / "__init__.py"

        if not init_module.exists():
            return False
        elif is_sgtk_pkg:
            return "__tk_pre_load = True" in init_module.read_text(encoding=sys.getdefaultencoding()).splitlines()
        else:
            return True

    def register_frameworks(self, engine: Engine):
        """Pre-Load all frameworks defined in current environment as standard python packages.

        Allow to import and use them as standard python package.
        """
        env = engine.get_env()

        frameworks_to_load = {}
        for framework_display_name in env.get_frameworks():
            frameworks_to_load.update(self._register_framework(env, framework_display_name))

        for f_pkg_name, (spec, mod) in frameworks_to_load.items():
            self.logger.info(f"Pre-Load framework: {f_pkg_name}")
            spec.loader.exec_module(mod)

    def _register_framework(self, env: InstalledEnvironment, name: str) -> dict[str, tuple[ModuleSpec, ModuleType]]:
        """Pre-Load framework as standard python packages. Allow to import and use them as standard python package."""
        f_descriptor: FrameworkDescriptor = env.get_framework_descriptor(name)
        f_path = Path(f_descriptor.get_path())

        f_pkg_name = self.conform_name(f_descriptor.system_name)

        if (f_path / f_pkg_name).exists():
            # Standard python package architecture
            root_path = f_path / f_pkg_name
        else:
            # SGTK Framework architecture (required for proper apps/framework/engine behavior)
            root_path = f_path / "python"

        spec_and_mod = self.register_package(f_pkg_name, root_path)
        if spec_and_mod:
            self.logger.info(f"Register framework: {f_pkg_name}")

            if self.need_preload(root_path):
                return {f_pkg_name: spec_and_mod}

        return {}

    def register_apps(self, engine: Engine):
        """Pre-Load all apps that uses standard python package structure."""
        for _, app in engine.apps.items():
            app_name = self.conform_name(app.name)

            app_path = Path(app.disk_location, app_name)

            if self.register_package(app_name, app_path):
                self.logger.info(f"Register app: {app_name}")

    def register_package(self, name: str, root_path: Path) -> tuple[ModuleSpec, ModuleType] | None:
        """Generic method to register a package into `sys.modules`."""
        init_module = root_path / "__init__.py"
        if init_module.exists():
            spec = importlib.util.spec_from_file_location(name, init_module)
            mod = importlib.util.module_from_spec(spec)

            sys.modules[name] = mod

            return spec, mod
        return None

    def patch_tank_bundle(self):
        """Patch a new import_module method in TankBundle class."""

        def import_module(self: TankBundle, module: str) -> ModuleType:
            """Try to return the module that was preloaded in `sys.modules`, fallback on the original TankBundle method if it fails."""
            try:
                return sys.modules[f"{self.name}.{module}"]
            except KeyError:
                return import_module._org(self, module)

        # --------------------------------------------------------------------------------------
        # --------------------------------------------------------------------------------------
        # --------------------------------------------------------------------------------------

        # Make sure all TankBundle references are trashed before patching
        for m in sys.modules.copy():
            if m.endswith(".platform.bundle"):
                sys.modules.pop(m)

        # Save original method as an attribute of the new one
        import_module._org = tank.platform.bundle.TankBundle.import_module

        # Patch both TankBundle access
        tank.platform.bundle.TankBundle.import_module = import_module
        sgtk.platform.bundle.TankBundle.import_module = import_module
