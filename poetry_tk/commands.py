#
# Copyright (c) 2022 Mathieu Bouzard.
#
# This file is part of poetry-tk
# (see https://gitlab.com/mathbou/poetry-tk).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
from __future__ import annotations

__all__ = [
    "FrameworkAddCommand",
    "FrameworkListCommand",
    "FrameworkSyncCommand",
    "FrameworkLoaderCommand",
    "FrameworkRemoveCommand",
    "FrameworkResolveCommand",
]

import shutil
from pathlib import Path
from typing import TYPE_CHECKING

import tomlkit
from cleo.helpers import argument, option
from poetry.console.commands.command import Command
from poetry.core.vcs.git import ParsedUrl
from poetry.utils.dependency_specification import parse_dependency_specification
from poetry.utils.env import EnvManager

from . import constants as cst
from . import framework as fk
from . import info
from .settings import Settings

if TYPE_CHECKING:
    from .framework import Framework


class FrameworkAddCommand(Command):
    name = "tk add"
    description = "Adds a new framework to environment."
    arguments = [argument("name", "The frameworks to add.", multiple=True)]
    options = [
        option("lock", description="Do not perform operations (only update the pyproject and info.yml)"),
    ]

    def _get_frameworks_to_install(self) -> dict[str, Framework]:
        frameworks = self.argument(cst.name)

        new_frameworks = {}
        for framework in frameworks:
            # TODO Add direct path support

            if framework.startswith("git+"):
                git = ParsedUrl.parse(framework)
                spec = {cst.name: git.name, "git": git.url}
                if git.rev:
                    spec[cst.version] = git.rev
            else:
                spec = parse_dependency_specification(framework)

            if "version" not in spec:
                spec[cst.version] = "*"

            framework = fk.Framework(**spec)
            new_frameworks[framework.name] = framework

        return new_frameworks

    def _update_config(self, new_frameworks: dict[str, Framework]):
        settings = Settings.load_from_pyproject(self.poetry.pyproject)

        try:
            tk_config = settings.data[cst.tool][cst.poetry_tk]
        except KeyError:
            tk_config = settings.data[cst.tool][cst.poetry_tk] = tomlkit.table(is_super_table=True)

        try:
            fk_config = tk_config[cst.frameworks]
        except KeyError:
            fk_config = tk_config[cst.frameworks] = tomlkit.table()
            existing_frameworks = {}
        else:
            existing_frameworks = {framework.name for framework in fk.load_from_pyproject(settings.frameworks)}

        duplicate_frameworks = existing_frameworks.intersection(list(new_frameworks))
        self.line(f"Already present frameworks are updated: {', '.join(duplicate_frameworks)}")

        for name, framework in new_frameworks.items():
            name, spec = framework.as_poetry()

            if len(spec) == 1:
                fk_config[name] = spec[cst.version]
            else:
                fk_config[name] = tomlkit.inline_table()
                fk_config[name].update(spec)

        if settings.update_info is True:
            info.update(settings, new_frameworks.values())

        self.poetry.pyproject.save()

    def handle(self) -> int:
        frameworks = self._get_frameworks_to_install()

        if not self.option("lock"):
            venv = EnvManager(self.poetry).get()
            if not venv.is_venv():
                raise RuntimeError("Poetry venv must be initialized to add frameworks.")

            fk.install(venv.path, frameworks.values())

        self._update_config(frameworks)

        self.line("Added: {0}".format(", ".join(list(frameworks))))
        return 0


class FrameworkRemoveCommand(Command):
    name = "tk remove"
    description = "Remove a framework from the environment."
    arguments = [argument("name", "The frameworks to remove.", multiple=True)]

    def handle(self) -> int:
        venv = EnvManager(self.poetry).get()
        if not venv.is_venv():
            raise RuntimeError("Poetry venv must be initialized to remove frameworks.")

        settings = Settings.load_from_pyproject(self.poetry.pyproject)

        fk_config = settings.frameworks
        frameworks = self.argument(cst.name)

        frameworks_to_remove = []
        for framework in frameworks:
            spec = parse_dependency_specification(framework)
            frameworks_to_remove.append(spec[cst.name])
            fk_config.pop(framework)

        fk.remove(venv.path, frameworks_to_remove)

        if settings.update_info is True:
            info.update(settings, fk.load_from_pyproject(settings.frameworks))

        self.poetry.pyproject.save()

        return 0


class FrameworkSyncCommand(Command):
    name = "tk sync"
    description = "Sync frameworks."
    options = [
        option("lock", description="Do not perform operations (only update the info.yml)"),
    ]

    def handle(self) -> int:
        venv = EnvManager(self.poetry).get()
        if not venv.is_venv():
            raise RuntimeError("Poetry venv must be initialized to properly sync frameworks.")

        settings = Settings.load_from_pyproject(self.poetry.pyproject)

        if not self.poetry.locker.is_fresh():
            if not self.confirm("Lockfile is not up-to-date. Continue anyway ?"):
                return 1

        sgtk_installed_packages = self.poetry.locker.locked_repository().search(cst.sgtk)
        if sgtk_installed_packages:
            self.line("tk-core already handled in standard project dependencies. Skip tk-core update.")
            core_version = str(sgtk_installed_packages[0].version)
            skip_core = True
        else:
            core_version = settings.core_version
            skip_core = False

        if not self.option("lock"):
            if not skip_core:
                core_version = fk.install_tk_core(self.poetry, core_version)

            fk.sync(settings.frameworks, venv.path)

        if settings.update_info is True:
            frameworks = fk.load_from_pyproject(settings.frameworks)
            info.update(settings, frameworks, core_version=core_version)

        return 0


class FrameworkLoaderCommand(Command):
    name = "tk install-loader"
    description = "Add framework loader to package."

    def handle(self) -> int:
        root = self.poetry.file.path.parent
        python_src = root / "python"

        python_src.mkdir(exist_ok=True)

        loader = Path(__file__).parent / "tk_scripts" / "__tk_loader.py"
        shutil.copy2(str(loader), str(python_src))
        self.line(f"Loader: {str(loader)} -> {str(python_src)}\n")

        init = python_src / "__init__.py"
        init.touch(exist_ok=True)

        cmd = "from . import __tk_loader  # Must be placed before any framework import."
        init_code = init.read_text(encoding="utf8").splitlines()

        if cmd not in init_code:
            init_code.insert(0, cmd)

        init.write_text("\n".join(init_code), encoding="utf8")

        return 0


class FrameworkListCommand(Command):
    name = "tk list"
    description = "List frameworks defined in this project."

    def handle(self) -> int:
        settings = Settings.load_from_pyproject(self.poetry.pyproject)
        for framework in fk.load_from_pyproject(settings.frameworks):
            self.line(f"{framework.name}: {framework.version}")

        return 0


class FrameworkResolveCommand(Command):
    name = "tk resolve"
    description = "Check current frameworks specs can be resolved."

    def handle(self) -> int:
        settings = Settings.load_from_pyproject(self.poetry.pyproject)

        frameworks = fk.load_from_pyproject(settings.frameworks)

        if not self.option("lock"):
            resolved_frameworks = fk.resolve(frameworks)

            for f in resolved_frameworks:
                self.line(f"{f.name} {f.last_allowed_version}")

        return 0
