This package is a plugin that allows the management of Shotgrid frameworks as standard python dependencies.
It clones the frameworks from their repository and link the python folder into the site-packages of the project venv.

# Installation

```shell
poetry self add https://gitlab.com/mathbou/poetry-tk.git
```

# Commands 

## tk add

```shell
Description:
  Adds a new framework to environment.

Usage:
  tk add [options] [--] <name>...

Arguments:
  name                  The frameworks to add.
  
Options:
  --lock            Do not perform operations (only update the pyproject and info.yml)
```

`<name>` supports two formats:
- name: Get the framework from Shotgun Software github organization.
- git url 

## tk sync

```shell
Description:
  Sync frameworks.

Usage:
  tk sync [options]
  
Options:
      --lock            Do not perform operations (only update the info.yml)
````

## tk list

```shell
Description:
  List frameworks defined in this project.

Usage:
  tk list [options]
```

## tk remove

```shell
Description:
  Remove a framework from the environment.

Usage:
  tk remove [options] [--] <name>...

Arguments:
  name                  The frameworks to remove.
```

## tk resolve

```shell
Description:
  Check current frameworks specs can be resolved.

Usage:
  tk resolve [options]
```

## tk install-loader

```shell
Description:
  Add framework loader to package.

Usage:
  tk install-loader [options]
```

# Configuration

## pyproject.toml

```toml
[tool.poetry-tk]
enable = true # (Default: False)
auto-sync = false # (Default: False) sync frameworks on `poetry install` calls
update-info = true # (Default: True) update framework specs in info.yml on poetry-tk add/sync/remove calls
core-version = "v0.20.0" # (Default: "*") Wich tk-core version will be installed as dev dependency 
```

```toml
[tool.poetry-tk.frameworks]
tk-framework-shotgunutils = "*" # Get the last version of framework from Shotgun Software github organization.

my-custom-framework = { git = "https://my.repo.git", version = "0.2.*,<0.2.5"} # Get the framework from Git url.

my-other-framework = { path = "C:/my/local/path/my-other-framework"} # Get the framework from local path.
```

---

# Tk-Loader

The [Tk-loader](#tk-install-loader) pre-load frameworks defined in the `info.yml` of a project using sgtk methods,  
but also exposes them as standard modules:

```python
# Instead of this
import sgtk
shotgun_model = sgtk.platform.import_framework("tk-framework-shotgunutils", "shotgun_model")
model = shotgun_model.ShotgunHierarchyModel()

# Or this (into apps/engines)
shotgun_model = self.frameworks["tk-framework-shotgunutils"].import_module("shotgun_model")
model = shotgun_model.ShotgunHierarchyModel()

# Do this
from tk_framework_shotgunutils.shotgun_model import ShotgunHierarchyModel
model = ShotgunHierarchyModel()

# Or this
from tk_framework_shotgunutils import shotgun_model
model = shotgun_model.ShotgunHierarchyModel()
```

## Installation

For the loader to properly work, it needs to be placed in the `python` folder of the project. 
Also, this line `from . import __tk_loader` is required in the `__init__.py` file

```yaml
📂 <app/framework/engine name>:
    - 📂 python:
        - 🐍 __init__.py: contains an import line for `__tk_loader`
        - 🐍 __tk_loader.py
        - 🐍 <your_module.py>
        - ...
    - 📝 info.yml:
    - ...           
```

> This procedure is taken care of by the [tk install-loader](#tk-install-loader) command.

# Tk EngineInit Hook

The `EngineInit` hook use the same base as the [Tk-Loader](#tk-loader) but extend it one step further:

- In opposition to the loader, this hook is not project specific, so it does not use `info.yml` information, 
instead it gets the **frameworks and apps** directly from the environment.
  - It registers all the frameworks and preload only those with `__tk_pre_load = True` in the `__init__.py` file
  - It registers only apps with a python package architecture. Sgtk apps will not be registered.
- The hook does not use sgtk loading methods, it only registers packages for standard imports.

## Package Struture

The hook allows those kinds of architectures for apps and frameworks.

### Sgtk

```yaml
📂 <app/framework name>:
    - 📂 python:
        - 🐍 __init__.py: contains the variable `__tk_pre_load = True`
        - 🐍 <your_module.py>
        - ...
    - 📝 info.yml
    - ...       
```

### Python package

In this case, the project would be package-able.

```yaml
📂 <app/framework name>:
    - 📂 python:
        - 🐍 __init__.py: contains the variable `__tk_pre_load = True`
    - 📂 <pkg_name>:
        - 🐍 __init__.py
        - 🐍 <your_module.py>
        - ...
    - 📝 info.yml
    - ...       
```

## Installation

It needs to be copied in your `tk-config` repo, in the `core/hooks` folder. 

